// Dodona
// Copyright (C) 2018 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <chrono>
#include <iostream>
#include <thread>

#include <aurora-controller/device.hpp>

int32_t main(int32_t argc_, char * argv_[])
{
  std::string ip_address;
  std::string token;

  std::cout << "Please enter ip address of aurora controller : ";
  std::cin >> ip_address;
  std::cout << "Please enter token of aurora controller      : ";
  std::cin >> token;

  Device device;
  device.set_ip_address(ip_address);
  device.set_token     (token     );

  auto state_on = device.get_state_on();
  if (state_on == false)
  {
    device.set_state_on(true);
  }

  state_on = device.get_state_on();
  if (state_on == true)
  {
    std::string dummy;
    std::cout << "By pressing enter the viewed command will be executed." << std::endl << std::endl;

    std::cout << "Set Brightness to 70 %." << std::endl;
    std::getline (std::cin, dummy);
    device.set_brightness_absolute(70);

    std::cout << "Increase Brightness by 30 %." << std::endl;
    std::getline (std::cin, dummy);
    device.set_brightness_delta(30);

    uint32_t brightness_stepwidth = 5;

    std::cout << "Decrease Brightness in " << brightness_stepwidth << " % steps to 0 by delta values and back to 100 % by absolute values." << std::endl;
    std::getline (std::cin, dummy);

    for (uint32_t i = 0; i < (100 / brightness_stepwidth); ++i)
    {
      std::cout << "i : " << i << std::endl;
      device.set_brightness_delta(-(brightness_stepwidth));
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }

    for (uint32_t i = 0; i < 100; i += brightness_stepwidth)
    {
      std::cout << "i : " << i << std::endl;
      device.set_brightness_absolute(i);
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }

    std::cout << "Determine current effect" << std::endl;
    std::getline (std::cin, dummy);
    std::cout << device.get_current_effect() << std::endl;

    std::cout << "Switch to next effect" << std::endl;
    std::getline (std::cin, dummy);
    device.switch_effect(1);

    std::cout << "Set hue to 0 degree and saturation 100 %." << std::endl;
    std::getline (std::cin, dummy);
    device.set_hue_absolute       (  0);
    device.set_saturation_absolute(100);

    uint32_t hue_step_width = 10;
    std::cout << "Increase hue from 0 degree to 360 degree in " << hue_step_width << " degree steps by delta values." << std::endl;
    std::getline (std::cin, dummy);

    for (uint32_t i = 0; i < 360; i += hue_step_width)
    {
      std::cout << "i : " << i << std::endl;
      device.set_hue_delta(hue_step_width);
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }

    std::cout << "Switch off device." << std::endl;
    std::getline (std::cin, dummy);
    device.set_state_on(false);

    return 0;
  }
  else
  {
    std::cout << "Could not communicate with aurora device. Are the given ip address and token correct?" << std::endl;
    return -1;
  }
}
