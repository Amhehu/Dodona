// Dodona
// Copyright (C) 2018 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <string>
#include <vector>

#include <rapidjson/document.h>

#include <curl/curl.h>

class Device
{
public :
  Device();
  ~Device();

  const std::string & get_ip_address() const;
  const std::string & get_name      () const;
  const std::string & get_token     () const;

  std::string get_all_infos() const;

  int32_t get_brightness       () const;
  int32_t get_color_temperature() const;
  int32_t get_hue              () const;
  int32_t get_saturation       () const;

  std::string              get_color_mode    () const;
  std::string              get_current_effect() const;
  std::vector<std::string> get_effect_list   () const;
  bool                     get_state_on      () const;

  void set_ip_address(const std::string & ip_address_);
  void set_token     (const std::string & token_     );
  void set_name      (const std::string & name_      );

  void set_brightness_absolute       (int32_t value_) const;
  void set_brightness_delta          (int32_t delta_) const;
  void set_color_temperature_absolute(int32_t value_) const;
  void set_color_temperature_delta   (int32_t delta_) const;
  void set_hue_absolute              (int32_t value_) const;
  void set_hue_delta                 (int32_t delta_) const;
  void set_saturation_absolute       (int32_t value_) const;
  void set_saturation_delta          (int32_t delta_) const;

  void set_value_absolute(int32_t value_, const std::string & channel_) const;
  void set_value_delta   (int32_t delta_, const std::string & channel_) const;

  void set_value(int32_t value_, bool absolute_, const std::string & channel_) const;

  void set_effect  (const std::string & effect_) const;
  void set_state_on(bool                state_ ) const;

  void switch_effect(int32_t delta_) const;

  void update_token();

protected :
  bool        get_bool  (const std::string & appendix_, const std::string & field_) const;
  int32_t     get_int   (const std::string & appendix_, const std::string & field_) const;
  std::string get_string(const std::string & appendix_, const std::string & field_) const;

  std::string _get_value(const std::string & appendix_) const;
  std::string _get_value(const std::string & url_, const std::string & appendix_) const;
  void _set_value(const std::string & appendix_, const std::string & post_field_) const;

  void _set_json_value(const std::string & post_field_, const rapidjson::Document & document_) const;

private :
  static uint32_t _number_devices;

  void _update_url();

  std::string _ip_address;
  std::string _name;
  std::string _token;
  std::string _url;
};
