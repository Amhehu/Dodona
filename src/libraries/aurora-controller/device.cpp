// Dodona
// Copyright (C) 2018 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>
#include <iostream>

#define RAPIDJSON_HAS_STDSTRING 1

#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

#include "device.hpp"

static const auto APPENDIX_API               = std::string("/api"        );
static const auto APPENDIX_BRIGHTNESS        = std::string("/brightness" );
static const auto APPENDIX_COLOR_MODE        = std::string("/color_mode" );
static const auto APPENDIX_COLOR_TEMPERATURE = std::string("/ct"         );
static const auto APPENDIX_EFFECTS           = std::string("/effects"    );
static const auto APPENDIX_EFFECTS_LIST      = std::string("/effectsList");
static const auto APPENDIX_HUE               = std::string("/hue"        );
static const auto APPENDIX_NEW               = std::string("/new"        );
static const auto APPENDIX_ON                = std::string("/on"         );
static const auto APPENDIX_SATURATION        = std::string("/sat"        );
static const auto APPENDIX_SELECT            = std::string("/select"     );
static const auto APPENDIX_STATE             = std::string("/state"      );
static const auto APPENDIX_V1                = std::string("/v1"         );

static const auto FIELD_BRIGHTNESS        = std::string("brightness");
static const auto FIELD_COLOR_TEMPERATURE = std::string("ct"        );
static const auto FIELD_INCREMENT         = std::string("increment" );
static const auto FIELD_HUE               = std::string("hue"       );
static const auto FIELD_ON                = std::string("on"        );
static const auto FIELD_SATURATION        = std::string("sat"       );
static const auto FIELD_SELECT            = std::string("select"    );
static const auto FIELD_VALUE             = std::string("value"     );

static const auto TYPE_GET  = std::string("GET" );
static const auto TYPE_POST = std::string("POST");
static const auto TYPE_PUT  = std::string("PUT" );

static const auto URL_HTTP = std::string("http://");
static const auto URL_PORT = std::string(":16021" );

size_t write_callback(char * data_, size_t size_, size_t number_, void * target_pointer_);
std::vector<std::string> split(const std::string & string_, char seperator_);

uint32_t Device::_number_devices = 0;

Device::Device() :
  _ip_address(),
  _name      (std::string("unnamed device")),
  _token     (),
  _url       ()
{
  if (_number_devices == 0)
  {
    curl_global_init(CURL_GLOBAL_ALL);
  }

  ++_number_devices;
}

Device::~Device()
{
  --_number_devices;

  if (_number_devices == 0)
  {
    curl_global_cleanup();
  }
}

const std::string & Device::get_ip_address() const
{
  return _ip_address;
}

const std::string & Device::get_name() const
{
  return _name;
}

const std::string & Device::get_token() const
{
  return _token;
}

std::string Device::get_all_infos() const
{
  return get_string(std::string(), std::string());
}

int32_t Device::get_brightness() const
{
  return get_int(APPENDIX_STATE + APPENDIX_BRIGHTNESS, FIELD_VALUE);
}

int32_t Device::get_color_temperature() const
{
  return get_int(APPENDIX_STATE + APPENDIX_COLOR_TEMPERATURE, FIELD_VALUE);
}

int32_t Device::get_hue() const
{
  return get_int(APPENDIX_STATE + APPENDIX_HUE, FIELD_VALUE);
}

int32_t Device::get_saturation() const
{
  return get_int(APPENDIX_STATE + APPENDIX_SATURATION, FIELD_VALUE);
}

std::string Device::get_color_mode() const
{
  return get_string(APPENDIX_STATE + APPENDIX_COLOR_MODE, std::string());
}

std::string Device::get_current_effect() const
{
  return get_string(APPENDIX_EFFECTS + APPENDIX_SELECT, std::string());
}

std::vector<std::string> Device::get_effect_list() const
{
  std::vector<std::string> list;

  std::string result = _get_value(APPENDIX_EFFECTS + APPENDIX_EFFECTS_LIST);
  if (result.size() > 0)
  {
    rapidjson::Document document;

    document.Parse(result.c_str());

    for (rapidjson::SizeType i = 0; i < document.Size(); ++i)
    {
      list.push_back(document[i].GetString());
    }
  }

  return list;
}

bool Device::get_state_on() const
{
  return get_bool(APPENDIX_STATE + APPENDIX_ON, FIELD_VALUE);
}

void Device::set_ip_address(const std::string & ip_address_)
{
  _ip_address = ip_address_;
  _update_url();
}

void Device::set_token(const std::string & token_)
{
  _token = token_;
  _update_url();
}

void Device::set_brightness_absolute(int32_t value_) const
{
  set_value_absolute(value_, FIELD_BRIGHTNESS);
}

void Device::set_brightness_delta(int32_t delta_) const
{
  set_value_delta(delta_, FIELD_BRIGHTNESS);
}

void Device::set_color_temperature_absolute(int32_t value_) const
{
  set_value_absolute(value_, FIELD_BRIGHTNESS);
}

void Device::set_color_temperature_delta(int32_t delta_) const
{
  set_value_delta(delta_, FIELD_BRIGHTNESS);
}

void Device::set_hue_absolute(int32_t value_) const
{
  set_value_absolute(value_, FIELD_HUE);
}

void Device::set_hue_delta(int32_t delta_) const
{
  set_value_delta(delta_, FIELD_HUE);
}

void Device::set_saturation_absolute(int32_t value_) const
{
  set_value_absolute(value_, FIELD_SATURATION);
}

void Device::set_saturation_delta(int32_t delta_) const
{
  set_value_delta(delta_, FIELD_SATURATION);
}

void Device::set_value_absolute(int32_t value_, const std::string & channel_) const
{
  set_value(value_, true, channel_);
}

void Device::set_value_delta(int32_t delta_, const std::string & channel_) const
{
  set_value(delta_, false, channel_);
}

void Device::set_value(int32_t value_, bool absolute_, const std::string & channel_) const
{
  rapidjson::Document document;
  document.SetObject();
  auto & allocator = document.GetAllocator();

  rapidjson::Value value;
  value.SetObject();
  value.AddMember(rapidjson::Value().SetString(absolute_ == true ? FIELD_VALUE : FIELD_INCREMENT, allocator), rapidjson::Value().SetInt(value_), allocator);

  document.AddMember(rapidjson::Value().SetString(channel_, allocator), value, allocator);

  _set_json_value(APPENDIX_STATE, document);
}

void Device::set_effect(const std::string & effect_) const
{
  rapidjson::Document document;
  document.SetObject();
  auto & allocator = document.GetAllocator();

  document.AddMember(rapidjson::Value().SetString(FIELD_SELECT, allocator), rapidjson::Value().SetString(effect_, allocator), allocator);

  _set_json_value(APPENDIX_EFFECTS, document);
}

void Device::set_name(const std::string & name_)
{
  _name = name_;
}

void Device::set_state_on(bool state_) const
{
  rapidjson::Document document;
  document.SetObject();
  auto & allocator = document.GetAllocator();

  rapidjson::Value value;
  value.SetObject();
  value.AddMember(rapidjson::Value().SetString(FIELD_VALUE , allocator), rapidjson::Value().SetBool(state_), allocator);

  document.AddMember(rapidjson::Value().SetString(FIELD_ON, allocator), value, allocator);

  _set_json_value(APPENDIX_STATE + APPENDIX_ON, document);
}

void Device::switch_effect(int32_t delta_) const
{
  auto current_effect = get_current_effect();
  auto effect_list    = get_effect_list   ();
  auto i_effect_list = std::find(effect_list.begin(), effect_list.end(), current_effect);

  auto position = (int32_t(std::distance(effect_list.begin(), i_effect_list)) + delta_) % effect_list.size();

  set_effect(effect_list[position]);
}

void Device::update_token()
{
  std::string result;

  auto url = URL_HTTP + _ip_address + URL_PORT + APPENDIX_API + APPENDIX_V1 + APPENDIX_NEW;

  auto curl = curl_easy_init();

  curl_easy_setopt(curl, CURLOPT_URL          , url.c_str()     );
  curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, TYPE_POST.c_str());
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_callback );
  curl_easy_setopt(curl, CURLOPT_WRITEDATA    , &result         );

  curl_easy_perform(curl);

  if (curl != nullptr)
  {
    curl_easy_cleanup(curl);
  }

  rapidjson::Document document;

  document.Parse(result.c_str());

  set_token(document["auth_token"].GetString());
}

bool Device::get_bool(const std::string & appendix_, const std::string & field_) const
{
  rapidjson::Document document;

  auto result = _get_value(appendix_);

  if (result.size() > 0)
  {
    document.Parse(result.c_str());

    if (field_.size() > 0)
    {
      return document[field_.c_str()].GetBool();
    }
    else
    {
      return document.GetBool();
    }
  }
  else
  {
    return false;
  }
}

int32_t Device::get_int(const std::string & appendix_, const std::string & field_) const
{
  rapidjson::Document document;

  auto result = _get_value(appendix_);

  if (result.size() > 0)
  {
    document.Parse(result.c_str());

    if (field_.size() > 0)
    {
      return document[field_.c_str()].GetInt();
    }
    else
    {
      return document.GetInt();
    }
  }
  else
  {
    return 0;
  }
}

std::string Device::get_string(const std::string & appendix_, const std::string & field_) const
{
  rapidjson::Document document;

  auto result = _get_value(appendix_);

  if (result.size() > 0)
  {
    document.Parse(result.c_str());

    if (field_.size() > 0)
    {
      return std::string(document[field_.c_str()].GetString());
    }
    else
    {
      if (document.IsString() == true)
      {
        return std::string(document.GetString());
      }
      else
      {
        return result;
      }
    }
  }
  else
  {
    return std::string();
  }
}

std::string Device::_get_value(const std::string & appendix_) const
{
  return _get_value(_url, appendix_);
}

std::string Device::_get_value(const std::string & url_, const std::string & appendix_) const
{
  std::string result;

  if (url_.length() > 0)
  {
    auto curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL          , (url_ + appendix_).c_str());
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, TYPE_GET.c_str()          );
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_callback           );
    curl_easy_setopt(curl, CURLOPT_WRITEDATA    , &result                   );

    curl_easy_perform(curl);

    if (curl != nullptr)
    {
      curl_easy_cleanup(curl);
    }
  }

  return result;
}

void Device::_set_json_value(const std::string & post_field_, const rapidjson::Document & document_) const
{
  rapidjson::StringBuffer string_buffer;
  rapidjson::Writer<rapidjson::StringBuffer> writer(string_buffer);
  document_.Accept(writer);

  _set_value(post_field_, string_buffer.GetString());
}

void Device::_set_value(const std::string & appendix_, const std::string & post_field_) const
{
  if (_url.length() > 0)
  {
    auto curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, (_url + appendix_).c_str());
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, TYPE_PUT.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_field_.c_str());

    curl_easy_perform(curl);

    if (curl != nullptr)
    {
      curl_easy_cleanup(curl);
    }
  }
}

void Device::_update_url()
{
  _url = URL_HTTP + _ip_address + URL_PORT + APPENDIX_API + APPENDIX_V1 + std::string("/") + _token;
}

size_t write_callback(char * data_, size_t size_, size_t number_, void * target_pointer_)
{
  auto as_string = static_cast<std::string *>(target_pointer_);

  if (as_string != nullptr)
  {
    as_string->clear();
    as_string->append((char *)(data_), size_ * number_);
  }

  return size_ * number_;
}

std::vector<std::string> split(const std::string & string_, char seperator_)
{
  std::vector<std::string> result;

  auto position = string_.find_first_of(seperator_, 0);
  auto current = decltype(position)(0);

  while (position != std::string::npos)
  {
    result.emplace_back(string_, current, position - current);
    current = position + 1;
    position = string_.find_first_of(seperator_, current);
  }
  result.emplace_back(string_, current);

  return result;
}
