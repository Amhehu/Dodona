// Dodona
// Copyright (C) 2018 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "vertical-label.hpp"

#include <QPainter>

VerticalLabel::VerticalLabel(const QString & text_, QWidget * parent_) :
  QLabel(text_, parent_)
{
}

void VerticalLabel::paintEvent(QPaintEvent *)
{
  QPainter painter(this);
  painter.translate(0,sizeHint().height());
  painter.rotate(270);

  painter.drawText(QRect(QPoint(0, 0), QLabel::sizeHint()), Qt::AlignCenter, text());
}

QSize VerticalLabel::minimumSizeHint() const
{
  auto s = QLabel::minimumSizeHint();
  return QSize(s.height(), s.width());
}

QSize VerticalLabel::sizeHint() const
{
  auto s = QLabel::sizeHint();
  return QSize(s.height(), s.width());
}
