// Dodona
// Copyright (C) 2018 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <QApplication>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QDomDocument>
#include <QtGlobal>
#include <QKeyEvent>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QStyleFactory>
#include <QTabWidget>
#include <QTextStream>

#include "dodona-window.hpp"
#include "vertical-label.hpp"

static const auto XML_DRIVES     = QString("drives"    );
static const auto XML_DRIVE      = QString("drive"     );
static const auto XML_NAME       = QString("name"      );
static const auto XML_TOKEN      = QString("token"     );
static const auto XML_IP_ADDRESS = QString("ip-address");

DodonaWindow::DodonaWindow(QWidget * parent_) :
  QWidget(parent_),
  _device_group(),
  _current_device_button(nullptr)
{
  _load_darktheme();
  installEventFilter(this);

  auto main_layout = new QVBoxLayout;

  auto manage_layout   = new QHBoxLayout;
  auto settings_layout = new QVBoxLayout;
  auto mode_widget     = new QTabWidget;

  auto new_device_button    = new QPushButton;

  new_device_button   ->setText(QString("add device"   ));
  _delete_device_button.setText(QString("delete device"));

  manage_layout->addWidget(new_device_button     );
  manage_layout->addWidget(&_delete_device_button);

  auto token_widget           = new QWidget;
  auto token_layout           = new QHBoxLayout;
  auto determine_token_button = new QPushButton;

  determine_token_button->setText(QString("determine"));

  token_layout->addWidget(&_edit_token          );
  token_layout->addWidget(determine_token_button);
  token_widget->setLayout(token_layout          );

  token_layout->setContentsMargins(QMargins(0, 0, 0, 0));

  auto connection_layout = new QFormLayout;

  connection_layout->addRow(QString("name"      ), &_edit_name      );
  connection_layout->addRow(QString("ip address"), &_edit_ip_address);
  connection_layout->addRow(QString("token"     ), token_widget     );

  auto state_layout = new QHBoxLayout;

  _state_on.setFixedSize(QSize(50, 20));
  _state_on.setCheckable(true);

  _brightness.setRange(0, 100);
  _brightness.setOrientation(Qt::Horizontal);

  state_layout->addWidget(&_state_on  );
  state_layout->addWidget(&_brightness);

  settings_layout->addLayout(connection_layout);
  settings_layout->addLayout(state_layout     );

  mode_widget->addTab(&_mode_effect_list, QString("effect list"));
  mode_widget->addTab(&_mode_color      , QString("color"      ));

  auto color_layout = new QHBoxLayout;

  _color_temperature.setOrientation(Qt::Vertical);
  _hue              .setOrientation(Qt::Vertical);
  _saturation       .setOrientation(Qt::Vertical);

  _color_temperature.setRange(1200, 6500);
  _hue              .setRange(   0,  360);
  _saturation       .setRange(   0,  100);

  auto color_temperature_label = new VerticalLabel(QString("color temperature"));
  auto hue_label               = new VerticalLabel(QString("hue"              ));
  auto saturation_label        = new VerticalLabel(QString("saturation"       ));

  color_layout->addWidget(color_temperature_label);
  color_layout->addWidget(&_color_temperature    );
  color_layout->addWidget(hue_label              );
  color_layout->addWidget(&_hue                  );
  color_layout->addWidget(saturation_label       );
  color_layout->addWidget(&_saturation           );

  _mode_color.setLayout(color_layout);

  main_layout->addLayout(manage_layout  );
  main_layout->addLayout(&_device_layout);
  main_layout->addLayout(settings_layout);
  main_layout->addWidget(mode_widget    );

  setLayout(main_layout);

  _device_button_toggled(nullptr, false);

  _state_on.setText(QString("off"));

  _load_settings();

  auto buttons = _device_group.buttons();
  if (buttons.size() > 0)
  {
    _device_button_toggled(buttons[0], true);
  }
  else
  {
    _new_device_clicked();
  }

  connect(new_device_button     , &QPushButton::clicked, this, &DodonaWindow::_new_device_clicked   );
  connect(&_delete_device_button, &QPushButton::clicked, this, &DodonaWindow::_delete_device_clicked);

  connect(determine_token_button, &QPushButton::clicked, this, &DodonaWindow::_determine_token_clicked);

  connect(&_device_group, QOverload<QAbstractButton *, bool>::of(&QButtonGroup::buttonToggled), this, &DodonaWindow::_device_button_toggled);
  connect(&_effect_list , QOverload<QAbstractButton *, bool>::of(&QButtonGroup::buttonToggled), this, &DodonaWindow::_effect_toggled       );

  connect(&_edit_name      , &QLineEdit::textEdited, this, &DodonaWindow::_update_main_device_settings);
  connect(&_edit_ip_address, &QLineEdit::textEdited, this, &DodonaWindow::_update_main_device_settings);
  connect(&_edit_token     , &QLineEdit::textEdited, this, &DodonaWindow::_update_main_device_settings);

  connect(&_brightness, &QSlider    ::valueChanged, this, &DodonaWindow::_brightness_changed);

  connect(&_color_temperature, &QSlider::valueChanged, this, &DodonaWindow::_color_temperature_changed);
  connect(&_hue              , &QSlider::valueChanged, this, &DodonaWindow::_hue_changed              );
  connect(&_saturation       , &QSlider::valueChanged, this, &DodonaWindow::_saturation_changed       );

  show();
}

DodonaWindow::~DodonaWindow()
{
  _save_settings();
}

DeviceButton * DodonaWindow::_new_device_clicked()
{
  auto device_button = new DeviceButton;
  device_button->setCheckable(true);
  device_button->setChecked  (true);
  _device_layout.addWidget(device_button);
  _device_group.addButton(device_button);
  _device_button_toggled(device_button, true);
  return device_button;
}

void DodonaWindow::_delete_device_clicked()
{
  if (_current_device_button != nullptr)
  {
    _device_layout.removeWidget(_current_device_button);
    delete _current_device_button;
    _device_button_toggled(nullptr, false);

    auto buttons = _device_group.buttons();
    if (buttons.size() > 0)
    {
      buttons[0]->setChecked(true);
    }
    else
    {
      _new_device_clicked();
    }
  }
}

void DodonaWindow::_determine_token_clicked()
{
  if (_current_device_button != nullptr)
  {
    auto & device = _current_device_button->get_device();

    QMessageBox::information(this,
                             QString("determine token"),
                             QString("please press the power on/off button the device for 5 - 7 seconds till the white light flashes."),
                             QMessageBox::Ok);
    device.update_token();
    _edit_token.setText(QString::fromStdString(device.get_token()));
  }
}

void DodonaWindow::_load_darktheme() const
{
  qApp->setStyle(QStyleFactory::create("Fusion"));

  QPalette dark_palette;
  dark_palette.setColor(QPalette::Window         , QColor( 53,  53,  53));
  dark_palette.setColor(QPalette::WindowText     , QColor(192, 192, 192));
  dark_palette.setColor(QPalette::Base           , QColor( 25,  25,  25));
  dark_palette.setColor(QPalette::AlternateBase  , QColor( 53,  53,  53));
  dark_palette.setColor(QPalette::ToolTipBase    , QColor(192, 192, 192));
  dark_palette.setColor(QPalette::ToolTipText    , QColor(192, 192, 192));
  dark_palette.setColor(QPalette::Text           , QColor(192, 192, 192));
  dark_palette.setColor(QPalette::Button         , QColor( 53,  53,  53));
  dark_palette.setColor(QPalette::ButtonText     , QColor(192, 192, 192));
  dark_palette.setColor(QPalette::BrightText     , QColor(192,   0,   0));
  dark_palette.setColor(QPalette::Link           , QColor( 30, 150,  80));
  dark_palette.setColor(QPalette::Highlight      , QColor( 30, 150,  80));
  dark_palette.setColor(QPalette::HighlightedText, QColor(  0,   0,   0));

  qApp->setPalette(dark_palette);
  qApp->setStyleSheet(QString("QToolTip                               \
                               {                                      \
                                 color           : #C0C0C0;           \
                                 background-color: #191919;           \
                                 border          : 1px solid #1E9650; \
                               }"));
}

bool DodonaWindow::eventFilter(QObject * target_, QEvent * event_)
{
  if (event_->type() == QEvent::KeyPress)
  {
    QKeyEvent * key_event = static_cast<QKeyEvent *>(event_);

    if (key_event->key() == Qt::Key_Escape)
    {
      close();
    }
  }
  return QWidget::eventFilter(target_, event_);
}

void DodonaWindow::_device_button_toggled(QAbstractButton * button_, bool toggled_)
{
  _current_device_button = (toggled_ == true) ? static_cast<DeviceButton *>(button_) : nullptr;

  _edit_name           .setEnabled(toggled_);
  _edit_ip_address     .setEnabled(toggled_);
  _edit_token          .setEnabled(toggled_);
  _brightness          .setEnabled(toggled_);
  _color_temperature   .setEnabled(toggled_);
  _hue                 .setEnabled(toggled_);
  _saturation          .setEnabled(toggled_);
  _state_on            .setEnabled(toggled_);
  _delete_device_button.setEnabled(toggled_);

  if ((_current_device_button != nullptr) && (toggled_ == true))
  {
    const auto & device = _current_device_button->get_device();

    _current_device_button->setText(QString::fromStdString((device.get_name())));

    _edit_name      .setText(QString::fromStdString((device.get_name      ())));
    _edit_ip_address.setText(QString::fromStdString((device.get_ip_address())));
    _edit_token     .setText(QString::fromStdString((device.get_token     ())));
  }
  else
  {
    _edit_name      .setText(QString());
    _edit_ip_address.setText(QString());
    _edit_token     .setText(QString());
  }

  _update_widgets();
}

void DodonaWindow::_update_widgets()
{
  bool state_on = false;

  int32_t brightness        = 100;
  int32_t color_temperature = 1200;
  int32_t hue               = 0;
  int32_t saturation        = 100;

  if (_current_device_button != nullptr)
  {
    auto & device = _current_device_button->get_device();

    if ((device.get_ip_address().size() > 0) && (device.get_token().size() > 0))
    {
      state_on = device.get_state_on();

      brightness        = device.get_brightness       ();
      color_temperature = device.get_color_temperature();
      hue               = device.get_hue              ();
      saturation        = device.get_saturation       ();
    }

    auto effect_list     = device.get_effect_list   ();
    auto current_effects = device.get_current_effect();

    auto effect_list_layout = new QVBoxLayout;

    for (const auto & i_effect_list : effect_list)
    {
      auto effect_button = new QRadioButton(QString::fromStdString(i_effect_list));
      effect_list_layout->addWidget(effect_button);
      _effect_list.addButton(effect_button);

      if (i_effect_list.compare(current_effects) == 0)
      {
        effect_button->setChecked(true);
      }
    }

    auto old_layout = _mode_effect_list.layout();
    if (old_layout != nullptr)
    {
      delete old_layout;
    }
    _mode_effect_list.setLayout(effect_list_layout);
  }
  else
  {
    auto effect_list_layout = _mode_effect_list.layout();
    if (effect_list_layout != nullptr)
    {
      delete effect_list_layout;
    }

    auto effects = _effect_list.buttons();
    for (const auto & i_effects : effects)
    {
      _effect_list.removeButton(i_effects);
      delete i_effects;
    }
  }

  disconnect(&_state_on, &QPushButton::toggled, this, &DodonaWindow::_state_on_toggled);
  _state_on.setChecked(state_on);
  connect(&_state_on, &QPushButton::toggled, this, &DodonaWindow::_state_on_toggled);

  _brightness       .setValue(brightness       );
  _color_temperature.setValue(color_temperature);
  _hue              .setValue(hue              );
  _saturation       .setValue(saturation       );
}

void DodonaWindow::_state_on_toggled(bool toggled_)
{
  if (_current_device_button != nullptr)
  {
    _state_on.setText(toggled_ == true ? QString("on") : QString("off"));
    _current_device_button->get_device().set_state_on(toggled_);
    _update_widgets();
  }
}

void DodonaWindow::_brightness_changed(int32_t value_)
{
  if (_current_device_button != nullptr)
  {
    _current_device_button->get_device().set_brightness_absolute(value_);
  }
}

void DodonaWindow::_effect_toggled(QAbstractButton * button_, bool toggled_)
{
  if ((button_ != nullptr) && (toggled_ == true))
  {
    auto & device = _current_device_button->get_device();

    auto s  = button_->text().toStdString();

    device.set_effect(s);
  }
}

void DodonaWindow::_color_temperature_changed(int32_t value_)
{
  if (_current_device_button != nullptr)
  {
    _current_device_button->get_device().set_color_temperature_absolute(value_);
  }
}

void DodonaWindow::_hue_changed(int32_t value_)
{
  if (_current_device_button != nullptr)
  {
    _current_device_button->get_device().set_hue_absolute(value_);
  }
}

void DodonaWindow::_saturation_changed(int32_t value_)
{
  if (_current_device_button != nullptr)
  {
    _current_device_button->get_device().set_saturation_absolute(value_);
  }
}

QString DodonaWindow::get_device_settings_path() const
{
  return qApp->applicationDirPath() + QString("/device-setting.xml");
}

void DodonaWindow::_load_settings()
{
  QFile file(get_device_settings_path());

  if (file.open(QIODevice::ReadOnly | QIODevice::Text) == true)
  {
    QDomDocument document;
    if (document.setContent(&file) == true)
    {
      auto drives_element = document.firstChildElement(XML_DRIVES);

      auto drive_nodes = drives_element.elementsByTagName(XML_DRIVE);

      for (int32_t i_drive_nodes = 0; i_drive_nodes < drive_nodes.count(); ++i_drive_nodes)
      {
        auto drive_node = drive_nodes.at(i_drive_nodes);
        if (drive_node.isElement() == true)
        {
          auto drive_element = drive_node.toElement();

          auto name       = drive_element.attribute(XML_NAME      );
          auto ip_address = drive_element.attribute(XML_IP_ADDRESS);
          auto token      = drive_element.attribute(XML_TOKEN     );

          auto device_button = _new_device_clicked();
          device_button->setText(name);

          auto & device = device_button->get_device();

          device.set_name      (name      .toStdString());
          device.set_ip_address(ip_address.toStdString());
          device.set_token     (token     .toStdString());
        }
      }
    }
  }
}

void DodonaWindow::_save_settings()
{
  QFile file(get_device_settings_path());

  if (file.open(QIODevice::WriteOnly | QIODevice::Text) == true)
  {
    QDomDocument document;

    document.appendChild(QDomProcessingInstruction(document.createProcessingInstruction("xml", "version='1.0' encoding='UTF-8'")));

    auto drives_element = document.createElement(XML_DRIVES);

    for (const auto & i_button_group : _device_group.buttons())
    {
      auto as_device_button = static_cast<DeviceButton *>(i_button_group);
      if (as_device_button != nullptr)
      {
        const auto & device = as_device_button->get_device();

        auto drive_element = document.createElement(XML_DRIVE);

        drive_element.setAttribute(XML_NAME      , QString::fromStdString(device.get_name      ()));
        drive_element.setAttribute(XML_IP_ADDRESS, QString::fromStdString(device.get_ip_address()));
        drive_element.setAttribute(XML_TOKEN     , QString::fromStdString(device.get_token     ()));

        drives_element.appendChild(drive_element);
      }
    }

    document.appendChild(drives_element);

    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    stream << document.toString();
    file.close();
  }
}

void DodonaWindow::_update_main_device_settings()
{
  if (_current_device_button != nullptr)
  {
    auto & device = _current_device_button->get_device();

    device.set_name      (_edit_name      .text().toStdString());
    device.set_ip_address(_edit_ip_address.text().toStdString());
    device.set_token     (_edit_token     .text().toStdString());

    _current_device_button->setText(_edit_name.text());
  }
}
