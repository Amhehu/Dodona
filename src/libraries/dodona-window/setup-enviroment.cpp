// Dodona
// Copyright (C) 2018 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "setup-enviroment.hpp"

#include <iostream>

#include <QApplication>
#include <QDir>
#include <QProcessEnvironment>

void setup_environment()
{
#ifdef WIN32
  auto system_environment = QProcessEnvironment::systemEnvironment();
  QString not_found("not found");
  auto qtdir = system_environment.value(QString("QTDIR"), not_found);

  if (qtdir.compare(not_found) != 0)
  {
    if (QDir(qtdir).exists() == true)
    {
      QString plugins_path = qtdir + QString("/plugins");
      if (QDir(plugins_path).exists() == true)
      {
        QCoreApplication::addLibraryPath(plugins_path);
      }
      else
      {
        std::cout << "platform path does not exist" << std::endl;
      }
    }
    else
    {
      std::cout << "found QTDIR path does not exist" << std::endl;
    }
  }
  else
  {
    std::cout << "QTDIR variable was not found" << std::endl;
  }
#endif
}
