// Dodona
// Copyright (C) 2018 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <vector>

#include <QButtonGroup>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QButtonGroup>
#include <QSlider>

#include "device-button.hpp"

class DodonaWindow : public QWidget
{
  Q_OBJECT

public :
  DodonaWindow(QWidget * parent_ = nullptr);
  ~DodonaWindow();

protected :
  void _load_darktheme() const;
  bool eventFilter(QObject * target_, QEvent * event_) override;

  void _update_widgets();

protected slots :
  DeviceButton * _new_device_clicked();
  void _delete_device_clicked();

  void _determine_token_clicked();

  void _device_button_toggled(QAbstractButton * button_, bool toggled_);
  void _update_main_device_settings();

  void _state_on_toggled(bool toggled_);
  void _effect_toggled(QAbstractButton * button_, bool toggled_);

  void _brightness_changed       (int32_t value_);
  void _color_temperature_changed(int32_t value_);
  void _hue_changed              (int32_t value_);
  void _saturation_changed       (int32_t value_);

  QString get_device_settings_path() const;

  void _load_settings();
  void _save_settings();

private :
  QButtonGroup _device_group;
  QHBoxLayout  _device_layout;

  QPushButton _delete_device_button;

  QLineEdit _edit_name      ;
  QLineEdit _edit_ip_address;
  QLineEdit _edit_token     ;

  QWidget _mode_effect_list;
  QWidget _mode_color      ;

  QPushButton _state_on         ;
  QSlider     _brightness       ;
  QSlider     _color_temperature;
  QSlider     _hue              ;
  QSlider     _saturation       ;

  QButtonGroup _effect_list;

  DeviceButton * _current_device_button;
};
