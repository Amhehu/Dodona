# Dodona
# Copyright (C) 2018 Silvio Tristram
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

CMAKE_MINIMUM_REQUIRED (VERSION 3.0.0)

IF (RapidJSON_INCLUDE_DIRS)
  SET (RapidJSON_FIND_QUIETLY TRUE)
ENDIF (RapidJSON_INCLUDE_DIRS)

FIND_PATH(RapidJSON_INCLUDE_DIRS NAMES rapidjson/rapidjson.h
  PATHS
    $ENV{RapidJSON_DIR}
    $ENV{RapidJSON_INCLUDE_DIR}
  PATH_SUFFIXES
    include
)

IF (RapidJSON_INCLUDE_DIRS)
  SET (RapidJSON_FOUND TRUE)
ELSE (RapidJSON_INCLUDE_DIRS)
  SET (RapidJSON_FOUND FALSE)
ENDIF (RapidJSON_INCLUDE_DIRS)