# Dodona
# Copyright (C) 2018 Silvio Tristram
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

CMAKE_MINIMUM_REQUIRED (VERSION 3.0.0)

IF (CURL_INCLUDE_DIRS AND CURL_LIBS)
  SET (CURL_FIND_QUIETLY TRUE)
ENDIF (CURL_INCLUDE_DIRS AND CURL_LIBS)

FIND_PATH (CURL_INCLUDE_DIR NAMES curl/curl.h
  PATHS
    $ENV{CURL_DIR}
    $ENV{CURL_INCLUDE_DIR}
  PATH_SUFFIXES
    include
)

FIND_LIBRARY (CURL_LIBRARY_DEBUG NAMES libcurl-d_imp
  PATHS
    $ENV{CURL_DIR}
    $ENV{CURL_LIB_DIR}
  PATH_SUFFIXES
    lib
    lib/Debug
)

FIND_LIBRARY (CURL_LIBRARY_RELEASE NAMES libcurl_imp
  PATHS
    $ENV{CURL_DIR}
    $ENV{CURL_LIB_DIR}
  PATH_SUFFIXES
    lib
    lib/Release
)

IF (CURL_LIBRARY_DEBUG)
  SET (CURL_LIBRARIES_TEMP debug ${CURL_LIBRARY_DEBUG})
ENDIF (CURL_LIBRARY_DEBUG)

IF (CURL_LIBRARY_RELEASE)
  SET (CURL_LIBRARIES_TEMP ${CURL_LIBRARIES_TEMP} optimized ${CURL_LIBRARY_RELEASE})
ENDIF (CURL_LIBRARY_RELEASE)

IF(CURL_LIBRARIES_TEMP)
  SET (CURL_LIBRARIES ${CURL_LIBRARIES_TEMP} ${CMAKE_THREAD_LIBS_INIT} CACHE STRING "CURL Bibliotheken")
ENDIF(CURL_LIBRARIES_TEMP)

IF (CURL_INCLUDE_DIR AND CURL_LIBRARIES)
  SET (CURL_FOUND TRUE)
ELSE (CURL_INCLUDE_DIR AND CURL_LIBRARIES)
  SET (CURL_FOUND FALSE)
ENDIF (CURL_INCLUDE_DIR AND CURL_LIBRARIES)